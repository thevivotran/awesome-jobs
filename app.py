from flask import Flask
import os
import sqlite3

path = os.path.abspath(os.path.dirname(__file__))
app = Flask("AwesomeJobs", template_folder=os.path.join(path, 'templates'))
jobs_list = []
with sqlite3.connect(os.path.join(path, 'jobs.db')) as conn:
    c = conn.cursor()
    for row in c.execute('SELECT * FROM jobs'):
        jobs_list.append(row)

from flask import render_template
@app.route("/")
def jobs():
    return render_template('index.html', jobs=jobs_list)
